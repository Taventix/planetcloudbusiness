package net.tavania.planetcloudbusiness.database;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.planetcloudbusiness.database
    Date: 04.11.2019
    
*/
public enum DatabaseType {

    MySQL,
    Redis;

}
