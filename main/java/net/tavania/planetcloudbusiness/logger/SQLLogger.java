package net.tavania.planetcloudbusiness.logger;

import net.tavania.planetcloudbusiness.main.PlanetCloud;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.CompletableFuture;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness - Server/net.tavania.planetcloudbusiness.logger
    Date: 13.12.2019
    
*/
public class SQLLogger {

    public static SQLLogger instance = new SQLLogger();

    public static SQLLogger getInstance() {
        return instance;
    }

    /**
     * Logging into the database
     *
     * @param loggerType the priority
     * @param message    custom message for the database
     */
    public void log(LoggerType loggerType, String message) {
        Date date = new Date();
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setGregorianChange(date);
        if (PlanetCloud.getInstance().getMySQLConnectionHandler().c != null) {
            CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getLogSQLHandler().createLog(PlanetCloud.getInstance().getLogSQLHandler().countAll() + 1, gregorianCalendar.getTime().toString(), loggerType + " | " + message));
        }
    }

}
