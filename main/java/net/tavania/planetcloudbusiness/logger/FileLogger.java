package net.tavania.planetcloudbusiness.logger;

import net.tavania.planetcloudbusiness.main.PlanetCloud;
import org.simpleyaml.configuration.file.YamlFile;
import org.simpleyaml.exceptions.InvalidConfigurationException;

import java.io.IOException;
import java.util.Date;
import java.util.GregorianCalendar;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.planetcloudbusiness.logger
    Date: 07.11.2019
    
*/
public class FileLogger {

    public static FileLogger instance = new FileLogger();

    public static FileLogger getInstance() {
        return instance;
    }

    /**
     * Logging to the file
     *
     * @param loggerType the priority
     * @param message    custom message
     */
    public void log(LoggerType loggerType, String message) {
        Date date = new Date();
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setGregorianChange(date);
        YamlFile yamlFile = new YamlFile(PlanetCloud.getInstance().getLocation() + "/Controller/log.yml");
        try {
            yamlFile.load();
            yamlFile.set(String.valueOf(gregorianCalendar.getTime()), message);
            yamlFile.save();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
