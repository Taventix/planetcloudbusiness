package net.tavania.planetcloudbusiness.logger;

import java.util.Date;
import java.util.GregorianCalendar;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.logger
    Date: 29.09.2019
    
*/
public class ConsoleLogger {

    public static ConsoleLogger instance = new ConsoleLogger();

    public static ConsoleLogger getInstance() {
        return instance;
    }

    /**
     * Logging into the console
     *
     * @param loggerType the priority
     * @param message           custom message for the console
     */
    public void log(LoggerType loggerType, String message) {
        Date date = new Date();
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setGregorianChange(date);
        System.out.println("| " + loggerType + " • " + gregorianCalendar.getTime() + " | " + message);
    }

}
