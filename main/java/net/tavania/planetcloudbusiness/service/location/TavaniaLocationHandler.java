package net.tavania.planetcloudbusiness.service.location;

import net.tavania.planetcloudbusiness.main.PlanetCloud;

import java.util.concurrent.CompletableFuture;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness - Server/net.tavania.planetcloudbusiness.service.location
    Date: 21.12.2019
    
*/
public class TavaniaLocationHandler {

    /**
     * Creating a Location asynchronous
     *
     * @param id      Location-id
     * @param name    Location-name
     * @param country Location-privacy
     */
    public void createLocationAsync(int id, String name, String country) {
        CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getLocationSQLHandler().createLocation(id, name, country));
    }

    /**
     * Getting a value asynchronous
     *
     * @param key database-key
     * @param id  Location-id
     * @throws Exception needed when key is not found
     */
    public void getAsyncById(String key, int id) throws Exception {
        switch (key) {
            case "name":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getLocationSQLHandler().getName(id));
            case "country":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getLocationSQLHandler().getCountry(id));
                break;
        }
    }

    /**
     * Getting a value asynchronous
     *
     * @param key  database-key
     * @param name Location-name
     * @throws Exception needed when key is not found
     */
    public void getAsyncByName(String key, String name) throws UnknownError {
        switch (key) {
            case "id":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getLocationSQLHandler().getId(name));
            case "country":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getLocationSQLHandler().getCountry(name));
                break;
        }
    }

    /**
     * Setting a value asynchronous
     *
     * @param key   database-key
     * @param name  Location-name
     * @param value database-value
     * @throws Exception needed when key is not found
     */
    public void setAsnycByName(String key, String name, String value) throws Exception {
        switch (key) {
            case "id":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getLocationSQLHandler().setId(name, Integer.parseInt(value)));
            case "country":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getLocationSQLHandler().setCountry(name, value));
                break;
        }
    }

    /**
     * Setting a value asynchronous
     *
     * @param key   database-key
     * @param id    Location-id
     * @param value database-value
     * @throws Exception needed when key is not found
     */
    public void setAsyncById(String key, int id, String value) throws Exception {
        switch (key) {
            case "id":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getLocationSQLHandler().setName(id, value));
            case "country":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getLocationSQLHandler().setCountry(id, value));
                break;
        }
    }

    /**
     * Getting all entries asynchronous
     */
    public void getAllAsync() {
        CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getLocationSQLHandler().getAll());
    }

    /**
     * Deleting an entry asynchronous
     *
     * @param name Location-name
     */
    public void deleteAsyncByName(String name) {
        CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getLocationSQLHandler().delete(name));
    }

    /**
     * Deleting an entry asynchronous
     *
     * @param id Location-id
     */
    public void deleteAsyncById(int id) {
        CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getLocationSQLHandler().delete(id));
    }

}
