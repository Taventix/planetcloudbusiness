package net.tavania.planetcloudbusiness.service;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.planetcloudbusiness.service
    Date: 07.10.2019
    
*/
public enum Privacy {

    PRIVATE,
    INTERNAL,
    PUBLIC;

}
