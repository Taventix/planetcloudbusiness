package net.tavania.planetcloudbusiness.service.project;

import net.tavania.planetcloudbusiness.main.PlanetCloud;

import java.util.concurrent.CompletableFuture;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.planetcloudbusiness.service
    Date: 07.10.2019
    
*/
public class TavaniaProjectHandler {

    /**
     * Creating a project asynchronous
     *
     * @param id      project-id
     * @param name    project-name
     * @param privacy project-privacy
     * @param date    project-date
     */
    public void createProjectAsync(int id, String name, String privacy, String date) {
        CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getProjectSQLHandler().createProject(id, name, privacy, date));
    }

    /**
     * Getting a value asynchronous
     *
     * @param key database-key
     * @param id  project-id
     * @throws Exception needed when key is not found
     */
    public void getAsyncById(String key, int id) throws Exception {
        switch (key) {
            case "name":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getProjectSQLHandler().getName(id));
            case "privacy":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getProjectSQLHandler().getPrivacy(id));
            case "date":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getProjectSQLHandler().getDate(id));
                break;
        }
    }

    /**
     * Getting a value asynchronous
     *
     * @param key  database-key
     * @param name project-name
     * @throws Exception needed when key is not found
     */
    public void getAsyncByName(String key, String name) throws UnknownError {
        switch (key) {
            case "id":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getProjectSQLHandler().getId(name));
            case "privacy":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getProjectSQLHandler().getPrivacy(name));
            case "date":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getProjectSQLHandler().getDate(name));
                break;
        }
    }

    /**
     * Setting a value asynchronous
     *
     * @param key   database-key
     * @param name  project-name
     * @param value database-value
     * @throws Exception needed when key is not found
     */
    public void setAsnycByName(String key, String name, String value) throws Exception {
        switch (key) {
            case "id":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getProjectSQLHandler().setId(name, Integer.valueOf(value)));
            case "privacy":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getProjectSQLHandler().setPrivacy(name, value));
            case "date":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getProjectSQLHandler().setDate(name, value));
                break;
        }
    }

    /**
     * Setting a value asynchronous
     *
     * @param key   database-key
     * @param id    project-id
     * @param value database-value
     * @throws Exception needed when key is not found
     */
    public void setAsyncById(String key, int id, String value) throws Exception {
        switch (key) {
            case "id":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getProjectSQLHandler().setName(id, value));
            case "privacy":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getProjectSQLHandler().setPrivacy(id, value));
            case "date":
                CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getProjectSQLHandler().setDate(id, value));
                break;
        }
    }

    /**
     * Getting all entries asynchronous
     */
    public void getAllAsync() {
        CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getProjectSQLHandler().getAll());
    }

    /**
     * Deleting an entry asynchronous
     *
     * @param name project-name
     */
    public void deleteAsyncByName(String name) {
        CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getProjectSQLHandler().delete(name));
    }

    /**
     * Deleting an entry asynchronous
     *
     * @param id project-id
     */
    public void deleteAsyncById(int id) {
        CompletableFuture.runAsync(() -> PlanetCloud.getInstance().getProjectSQLHandler().delete(id));
    }
}
