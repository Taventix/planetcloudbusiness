package net.tavania.planetcloudbusiness.service.project;

import net.tavania.planetcloudbusiness.service.Privacy;

import java.util.*;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.planetcloudbusiness.service
    Date: 07.10.2019
    
*/
public class TavaniaProject {

    public static HashMap<String, TavaniaProject> tavaniaProject = new HashMap<>();
    private String name;
    private int id;
    private Date date;
    private GregorianCalendar gregorianCalendar = new GregorianCalendar();
    public static List<TavaniaProject> tavaniaProjects = new ArrayList<>();
    private Privacy projectPrivacy;

    /**
     * Creating a new project
     *
     * @param id
     * @param name    is a variable project name
     * @param privacy is the specific security level
     * @param date    is the creation-date
     */
    public TavaniaProject(int id, String name, Privacy privacy, Date date) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.projectPrivacy = privacy;
        getGregorianCalendar().setGregorianChange(date);
        tavaniaProject.put(name, this);
        tavaniaProjects.add(this);
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public GregorianCalendar getGregorianCalendar() {
        return gregorianCalendar;
    }

    public static List<TavaniaProject> getTavaniaProjects() {
        return tavaniaProjects;
    }

    public static HashMap<String, TavaniaProject> getTavaniaProject() {
        return tavaniaProject;
    }

    public Privacy getProjectPrivacy() {
        return projectPrivacy;
    }
}
