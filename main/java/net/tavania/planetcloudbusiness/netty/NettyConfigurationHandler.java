package net.tavania.planetcloudbusiness.netty;

import net.tavania.planetcloudbusiness.logger.ConsoleLogger;
import net.tavania.planetcloudbusiness.logger.LoggerType;
import net.tavania.planetcloudbusiness.main.PlanetCloud;

import java.io.File;
import java.util.Scanner;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.planetcloudbusiness.netty
    Date: 04.11.2019
    
*/
public class NettyConfigurationHandler {

    private int transferSpeed;
    private String architecture;

    public void configureNetty() {
        setArchitecture("SERVER");
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "On which port should the server run?");
        Scanner scanner = new Scanner(System.in);
        String nextLine = scanner.nextLine();
        try {
            PlanetCloud.getInstance().getNettyServerConnection().setPort(Integer.parseInt(nextLine));
        } catch (NumberFormatException ex) {
            ConsoleLogger.getInstance().log(LoggerType.ERROR, "Please type in numbers.");
            return;
        }
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Thank you!");
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Please choose your transfer speed between the server and clients! [Bytes]");
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "1MB: 1 000 000 Bytes");
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "100MB: 100 000 000 Bytes");
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "1TB: 1  000 000 000 Bytes");
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "MAX. SPEED: 2 000 000 000 Bytes");
        Scanner scanner1 = new Scanner(System.in);
        String nextLine1 = scanner1.nextLine();
        try {
            setTransferSpeed(Integer.parseInt(nextLine1));
            ConsoleLogger.getInstance().log(LoggerType.SUCCESS, "Successfully set transfer speed to: " + nextLine1 + " bytes.");
        } catch (NumberFormatException ex) {
            ConsoleLogger.getInstance().log(LoggerType.ERROR, "Please type in numbers.");
        }
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Saving all data...");
        try {
            PlanetCloud.getInstance().getJsonWebDataHandler().setJsonString(PlanetCloud.getInstance().getLocation() + "/Controller/config.json", "Architecture", getArchitecture());
            PlanetCloud.getInstance().getJsonWebDataHandler().setJsonString(PlanetCloud.getInstance().getLocation() + "/Controller/config.json", "TransferSpeed", String.valueOf(getTransferSpeed()));
            PlanetCloud.getInstance().getJsonWebDataHandler().setJsonString(PlanetCloud.getInstance().getLocation() + "/Controller/config.json", "ServerPort", String.valueOf(PlanetCloud.getInstance().getNettyServerConnection().getPort()));
        } catch (Exception ex) {
            ConsoleLogger.getInstance().log(LoggerType.ERROR, "Could not save all files! Try again!");
            return;
        }
        ConsoleLogger.getInstance().log(LoggerType.SUCCESS, "Successfully saved all data!");
        new File("plugins").mkdir();
        return;
    }

    public int getTransferSpeed() {
        return transferSpeed;
    }

    public void setTransferSpeed(int transferSpeed) {
        this.transferSpeed = transferSpeed;
    }

    public String getArchitecture() {
        return architecture;
    }

    public void setArchitecture(String architecture) {
        this.architecture = architecture;
    }
}
