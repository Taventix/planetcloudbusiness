package net.tavania.planetcloudbusiness.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import net.tavania.planetcloudbusiness.logger.ConsoleLogger;
import net.tavania.planetcloudbusiness.logger.LoggerType;
import net.tavania.planetcloudbusiness.netty.handler.NettyServerHandler;
import net.tavania.planetcloudbusiness.user.ConnectDatabaseMethod;

import java.nio.charset.StandardCharsets;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.planetcloudbusiness.netty
    Date: 24.10.2019
    
*/
public class NettyServerConnection {

    private int port;
    private boolean keepAlive = true;
    private ChannelFuture channelFuture;
    private ChannelHandlerContext channelHandlerContext;

    public void startServer(int port, boolean keepAlive) {
        this.port = port;
        this.keepAlive = keepAlive;
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(bossGroup, workerGroup);
            bootstrap.channel(NioServerSocketChannel.class);
            bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel channel) throws Exception {
                    ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Client connected with IP: " + channel.remoteAddress().getHostName());
                    channel.pipeline().addLast(new StringEncoder(StandardCharsets.UTF_8),
                            new StringDecoder(StandardCharsets.UTF_8),
                            new NettyServerHandler());
                }
            });
            bootstrap.option(ChannelOption.SO_BACKLOG, 50);
            bootstrap.childOption(ChannelOption.SO_KEEPALIVE, isKeepAlive());
            channelFuture = bootstrap.bind(getPort()).sync();
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Netty-Server is listening...");
            new ConnectDatabaseMethod().run();
            ConsoleLogger.getInstance().log(LoggerType.SUCCESS, "Started PlanetCloud successfully!");
            channelFuture.channel().closeFuture().sync();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isKeepAlive() {
        return keepAlive;
    }

    public ChannelFuture getChannelFuture() {
        return channelFuture;
    }

    public ChannelHandlerContext getChannelHandlerContext() {
        return channelHandlerContext;
    }

    public void setChannel(ChannelHandlerContext channelHandlerContext) {
        this.channelHandlerContext = channelHandlerContext;
    }
}
