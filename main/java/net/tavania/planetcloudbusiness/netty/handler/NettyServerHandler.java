package net.tavania.planetcloudbusiness.netty.handler;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import net.tavania.planetcloudbusiness.launcher.thread.FileServerThread;
import net.tavania.planetcloudbusiness.logger.FileLogger;
import net.tavania.planetcloudbusiness.logger.LoggerType;
import net.tavania.planetcloudbusiness.logger.SQLLogger;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.planetcloudbusiness.netty.handler
    Date: 24.10.2019
    
*/
public class NettyServerHandler extends SimpleChannelInboundHandler<String> {


    private String clientMessage;

    /*
    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        ctx.channel().writeAndFlush("Welcome client!");
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Connection to client is ready!");
        //PlanetCloud.getInstance().getTavaniaThread().startThread(new FileServerThread(), "FileServerThread");
    }
     */
    public static final ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    @Override
    public void channelRead0(ChannelHandlerContext channelHandlerContext, String message) throws Exception {
        System.out.println("Packet received: " + message);
        FileLogger.getInstance().log(LoggerType.INFORMATION, "Received packet: " + message);
        SQLLogger.getInstance().log(LoggerType.INFORMATION, "Received packet: " + message);
        Channel incoming = channelHandlerContext.channel();
        for (Channel channel : channels) {
            if (channel != incoming) {
                channel.writeAndFlush("[" + incoming.remoteAddress() + "] " + message + "\n");
            }
        }
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        Channel incoming = ctx.channel();
        for (Channel channel : channels) {
            channel.writeAndFlush("[SERVER] - " + incoming.remoteAddress() + " has joined!\n");
        }
        SQLLogger.getInstance().log(LoggerType.INFORMATION, incoming.remoteAddress() + " has joined!");
        channels.add(ctx.channel());
        new FileServerThread().run();
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        Channel incoming = ctx.channel();
        for (Channel channel : channels) {
            channel.writeAndFlush("[SERVER] - " + incoming.remoteAddress() + " has left!\n");
        }
        SQLLogger.getInstance().log(LoggerType.INFORMATION, incoming.remoteAddress() + " has left!");
        channels.remove(ctx.channel());
        ctx.close();
    }

    public String getClientMessage() {
        return clientMessage;
    }

    public void setClientMessage(String clientMessage) {
        this.clientMessage = clientMessage;
    }
}
