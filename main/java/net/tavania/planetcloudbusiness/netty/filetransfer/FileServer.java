package net.tavania.planetcloudbusiness.netty.filetransfer;

import net.tavania.planetcloudbusiness.main.PlanetCloud;

import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.planetcloudbusiness.netty.filetransfer
    Date: 03.11.2019
    
*/
public class FileServer {

    private ServerSocket serverSocket;

    public FileServer(int port) {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        while (true) {
            try {
                Socket clientSock = serverSocket.accept();
                saveFile(clientSock);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void saveFile(Socket clientSock) throws IOException {
        DataInputStream dis = new DataInputStream(clientSock.getInputStream());
        FileOutputStream fos = new FileOutputStream("testfile.jpg");
        byte[] buffer = new byte[PlanetCloud.getInstance().getNettyConfigurationHandler().getTransferSpeed()];

        int filesize = PlanetCloud.getInstance().getNettyConfigurationHandler().getTransferSpeed(); // Send file size in separate msg
        int read = 0;
        int totalRead = 0;
        int remaining = filesize;
        while ((read = dis.read(buffer, 0, Math.min(buffer.length, remaining))) > 0) {
            totalRead += read;
            remaining -= read;
            //System.out.println("read " + totalRead + " bytes.");
            fos.write(buffer, 0, read);
        }

        fos.close();
        dis.close();
    }


}
