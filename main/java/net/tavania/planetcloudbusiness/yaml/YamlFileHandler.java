package net.tavania.planetcloudbusiness.yaml;

import net.tavania.planetcloudbusiness.main.PlanetCloud;
import org.simpleyaml.configuration.file.YamlFile;
import org.simpleyaml.exceptions.InvalidConfigurationException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.planetcloudbusiness.yaml
    Date: 11.10.2019
    
*/
public class YamlFileHandler {

    /**
     * Get a file string by using YAML
     *
     * @param filepath   is the file-key
     * @param stringpath is the file-objectr
     * @return a string
     */
    public String getFileString(String filepath, String stringpath) {
        YamlFile yamlFile = new YamlFile(filepath);
        try {
            yamlFile.load();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return yamlFile.getString(stringpath);
    }

    /**
     * Copy a file in another directory
     *
     * @param in  is the input-file
     * @param out is the output-file
     * @throws IOException necessary because of the IOException
     */
    public void copyFile(File in, File out) throws IOException {
        FileChannel inChannel = null;
        FileChannel outChannel = null;
        try {
            inChannel = new FileInputStream(in).getChannel();
            outChannel = new FileOutputStream(out).getChannel();
            inChannel.transferTo(0, inChannel.size(), outChannel);
        } catch (IOException e) {
            throw e;
        } finally {
            try {
                if (inChannel != null)
                    inChannel.close();
                if (outChannel != null)
                    outChannel.close();
            } catch (IOException e) {
            }
        }
    }

    /**
     * Creating the default directory-structure
     *
     * @param location
     */
    public void createDefaultDirectoryStructure(final String location) {
        // Directories
        File locationFolder = new File(location);
        File controllerFolder = new File(location + "/Controller");
        File projectFolder = new File(location + "/Projects");
        File userFolder = new File(location + "/Users");

        if (!locationFolder.exists()) {
            locationFolder.mkdir();
            controllerFolder.mkdir();
            projectFolder.mkdir();
            userFolder.mkdir();
        }

        // Files
        PlanetCloud.getInstance().getJsonWebDataHandler().createJsonFile(location + "/Controller/config.json");
        PlanetCloud.getInstance().getJsonWebDataHandler().createJsonFile(location + "/Controller/authkeys.json");
        PlanetCloud.getInstance().getJsonWebDataHandler().createJsonFile(location + "/Controller/usertemplates.json");
        PlanetCloud.getInstance().getJsonWebDataHandler().createJsonFile(location + "/Controller/projects.json");
        PlanetCloud.getInstance().getJsonWebDataHandler().createJsonFile("locations.json");
        YamlFile yamlFile = new YamlFile(location + "/Controller/log.yml");
        try {
            if (!yamlFile.exists()) {
                yamlFile.createNewFile(false);
                yamlFile.set("TavaniaCloud", "Welcome to the log!");
                yamlFile.save();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
