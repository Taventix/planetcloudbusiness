package net.tavania.planetcloudbusiness.launcher.thread;

import net.tavania.planetcloudbusiness.logger.ConsoleLogger;
import net.tavania.planetcloudbusiness.logger.LoggerType;
import net.tavania.planetcloudbusiness.main.PlanetCloud;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.launcher.thread
    Date: 06.10.2019
    
*/
public class UpdateThread implements Runnable {

    @Override
    public void run() {
        PlanetCloud.getInstance().getJsonWebDataHandler().getDataFromWesbite("https://tavania.net/planetcloud/version.json");
        String latestVersion = PlanetCloud.getInstance().getJsonWebDataHandler().getJsonObject().getString("business-version");
        String installedVersion = PlanetCloud.VERSION;
        if (!latestVersion.equalsIgnoreCase(installedVersion)) {
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Threre is a new update available: https://tavania.net/planetcloud");
        } else {
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "No updates available.");
        }
    }
}
