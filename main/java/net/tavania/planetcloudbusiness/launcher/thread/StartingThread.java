package net.tavania.planetcloudbusiness.launcher.thread;

import net.tavania.planetcloudbusiness.main.PlanetCloud;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.launcher.thread
    Date: 06.10.2019
    
*/
public class StartingThread implements Runnable {

    @Override
    public void run() {
        String currentLocation = PlanetCloud.getInstance().getJsonWebDataHandler().getString("locations.json", "CurrentCity");
        PlanetCloud.getInstance().setLocation(currentLocation);
        if (PlanetCloud.getInstance().getJsonWebDataHandler().getString(PlanetCloud.getInstance().getLocation() + "/Controller/config.json", "DatabaseType").equalsIgnoreCase("MySQL")) {
            PlanetCloud.getInstance().getMySQLConnectionHandler().connect();
        } else if (PlanetCloud.getInstance().getJsonWebDataHandler().getString(PlanetCloud.getInstance().getLocation() + "/Controller/config.json", "DatabaseType").equalsIgnoreCase("Redis")) {
            PlanetCloud.getInstance().getRedisConnectionHandler().connect();
        }
    }


}
