package net.tavania.planetcloudbusiness.launcher.thread;

import net.tavania.planetcloudbusiness.logger.ConsoleLogger;
import net.tavania.planetcloudbusiness.logger.LoggerType;

import java.util.ArrayList;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.launcher.thread
    Date: 06.10.2019
    
*/
public class TavaniaThread {

    private ArrayList<Thread> runningThreads = new ArrayList<>();
    private Thread thread;

    /**
     * Starting a thread
     *
     * @param runnable   is needed class
     * @param threadName is the custom name
     */
    public void startThread(Runnable runnable, String threadName) {
        thread = new Thread(runnable);
        thread.setName(threadName);
        thread.run();
        runningThreads.add(thread);
        ConsoleLogger.getInstance().log(LoggerType.SUCCESS, "Started thread '" + threadName + "' successfully.");
    }

    /**
     * Stopping a thread
     *
     * @param threadName is the custom thread name, defined in the start method
     */
    public void stopThread(String threadName) {
        getRunningThreads().remove(getThread(threadName));
        getThread(threadName).stop();
        ConsoleLogger.getInstance().log(LoggerType.SUCCESS, "Stopped thread '" + threadName + "' successfully.");
    }

    /**
     * Getting a thread
     *
     * @param threadName is the custom name
     * @return a thread by using the java thread class
     */
    public Thread getThread(String threadName) {
        for (java.lang.Thread t : java.lang.Thread.getAllStackTraces().keySet()) {
            if (t.getName().equals(threadName)) return t;
        }
        return null;
    }

    /**
     * Getting all running threads
     *
     * @return all running threads
     */
    public ArrayList<Thread> getRunningThreads() {
        return runningThreads;
    }

    public Thread getThread() {
        return thread;
    }
}
