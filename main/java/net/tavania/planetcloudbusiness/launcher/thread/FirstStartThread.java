package net.tavania.planetcloudbusiness.launcher.thread;

import com.google.gson.Gson;
import net.tavania.planetcloudbusiness.logger.ConsoleLogger;
import net.tavania.planetcloudbusiness.logger.LoggerType;
import net.tavania.planetcloudbusiness.main.PlanetCloud;
import net.tavania.planetcloudbusiness.service.location.TavaniaLocation;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.planetcloudbusiness.launcher.thread
    Date: 21.10.2019
    
*/
public class FirstStartThread implements Runnable {

    @Override
    public void run() {
        //Authentication
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Welcome to TavaniaCloud. I see it's your first time?!");
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "First, you need to authorize because of security reasons.");
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "1. Download the Google Authenticator App in your AppStore on your phone.");
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "2. Click on 'new key' and type in the following code:");
        PlanetCloud.getInstance().getGoogleAuthenticationHandler().a();
        Scanner authScanner = new Scanner(System.in);
        String authNextLine = authScanner.nextLine();
        PlanetCloud.getInstance().getGoogleAuthenticationHandler().b(Integer.parseInt(authNextLine));
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Do you want to enable 2FA in future login's? (true, false)");
        Scanner scanner3 = new Scanner(System.in);
        boolean nextInput3 = true;
        try {
            nextInput3 = scanner3.nextBoolean();
        } catch (Exception ex) {
            ConsoleLogger.getInstance().log(LoggerType.ERROR, "Invalid answer! Changed automatically to: YES");
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "You can change that manual in the config.json file.");
        }
        ConsoleLogger.getInstance().log(LoggerType.SUCCESS, "Thank you! Changed to: " + nextInput3);
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Please type in your city, you can change the name later as well.");
        Scanner scanner = new Scanner(System.in);
        String nextInput = scanner.nextLine();
        PlanetCloud.getInstance().setCurrentLocationName(nextInput);
        PlanetCloud.getInstance().setLocation(nextInput);
        PlanetCloud.getInstance().getYamlFileHandler().createDefaultDirectoryStructure(nextInput);
        PlanetCloud.getInstance().getJsonWebDataHandler().setJsonString("locations.json", "CurrentCity", nextInput);
        ConsoleLogger.getInstance().log(LoggerType.SUCCESS, "Thank you! Your city is: " + PlanetCloud.getInstance().getCurrentLocationName());

        PlanetCloud.getInstance().getJsonWebDataHandler().setJsonBoolean(PlanetCloud.getInstance().getCurrentLocationName() + "/Controller/config.json", "2FA", nextInput3);
        System.out.println(PlanetCloud.getInstance().getGoogleAuthenticationHandler().getGoogleAuthKey());
        PlanetCloud.getInstance().getJsonWebDataHandler().setJsonString(PlanetCloud.getInstance().getCurrentLocationName() + "/Controller/authkeys.json", "DefaultKey", PlanetCloud.getInstance().getGoogleAuthenticationHandler().getGoogleAuthKey());

        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Please type in your country, you can change the name later as well.");
        Scanner scanner1 = new Scanner(System.in);
        String nextInput1 = scanner1.nextLine();
        TavaniaLocation tavaniaLocation = new TavaniaLocation(TavaniaLocation.getTavaniaLocations().size() + 1, nextInput, nextInput1);
        Gson gson = new Gson();
        String json = gson.toJson(tavaniaLocation);
        PlanetCloud.getInstance().getJsonWebDataHandler().setJsonString("locations.json", nextInput, json);
        ConsoleLogger.getInstance().log(LoggerType.SUCCESS, "Thank you! Your country is: " + nextInput1);
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Don't worry, i will do anything ready for you! Please wait a second...");
        InetAddress ipAddress = null;
        try {
            ipAddress = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Saving your ipAddress (" + ipAddress.getHostAddress() + ")...");
        PlanetCloud.getInstance().getJsonWebDataHandler().setJsonString(PlanetCloud.getInstance().getCurrentLocationName() + "/Controller/authkeys.json", "HostAddress", ipAddress.getHostAddress());
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Saving your operating system (" + System.getProperty("os.name") + ")");
        PlanetCloud.getInstance().getJsonWebDataHandler().setJsonString(PlanetCloud.getInstance().getCurrentLocationName() + "/Controller/config.json", "OperatingSystem", System.getProperty("os.name"));
        PlanetCloud.getInstance().getJsonConfigurationHandler().configureDatabase();
        PlanetCloud.getInstance().getNettyConfigurationHandler().configureNetty();
    }

}
