package net.tavania.planetcloudbusiness.launcher.thread;

import net.tavania.planetcloudbusiness.netty.filetransfer.FileServer;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.planetcloudbusiness.launcher.thread
    Date: 03.11.2019
    
*/
public class FileServerThread implements Runnable {
    @Override
    public void run() {
        FileServer fs = new FileServer(1988);
        fs.run();
    }
}
