package net.tavania.planetcloudbusiness.launcher.command;

import net.tavania.planetcloudbusiness.logger.ConsoleLogger;
import net.tavania.planetcloudbusiness.logger.LoggerType;
import net.tavania.planetcloudbusiness.main.PlanetCloud;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness - Server/net.tavania.planetcloudbusiness.launcher.command
    Date: 21.12.2019
    
*/
public class StatusCommand extends Command {
    public StatusCommand(String name, String... alias) {
        super(name, alias);
    }

    @Override
    public void execute(ConsoleLogger logger, String name, String... args) {
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "");
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Current status of TavaniaCloud:");
        if (PlanetCloud.getInstance().getMySQLConnectionHandler().c != null) {
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Database: CONNECTED");
        } else {
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Database: DISCONNECTED");
        }
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, PlanetCloud.getInstance().getTavaniaThread().getRunningThreads().size() + " Threads running");
        if (PlanetCloud.getInstance().getNettyServerConnection().getChannelFuture() != null) {
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "NettyServer: CONNECTED");
        } else {
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "NettyServer: DISCONNECTED");
        }
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "");
    }
}
