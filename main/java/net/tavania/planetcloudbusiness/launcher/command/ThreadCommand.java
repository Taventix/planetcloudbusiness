package net.tavania.planetcloudbusiness.launcher.command;

import net.tavania.planetcloudbusiness.logger.ConsoleLogger;
import net.tavania.planetcloudbusiness.logger.LoggerType;
import net.tavania.planetcloudbusiness.main.PlanetCloud;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness - Server/net.tavania.planetcloudbusiness.launcher.command
    Date: 21.12.2019
    
*/
public class ThreadCommand extends Command {
    public ThreadCommand(String name, String... alias) {
        super(name, alias);
    }

    @Override
    public void execute(ConsoleLogger logger, String name, String... args) {
        if (PlanetCloud.getInstance().getTavaniaThread().getRunningThreads().size() != 0) {
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "There are currently " + PlanetCloud.getInstance().getTavaniaThread().getRunningThreads().size() + " Threads running.");
            for (int i = 0; i < PlanetCloud.getInstance().getTavaniaThread().getRunningThreads().size(); i++) {
                ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Thread: " + PlanetCloud.getInstance().getTavaniaThread().getRunningThreads().get(i).getName() + " with priority: " + PlanetCloud.getInstance().getTavaniaThread().getRunningThreads().get(i).getPriority());
            }
        } else {
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "There are currently no threads running.");
        }
    }
}
