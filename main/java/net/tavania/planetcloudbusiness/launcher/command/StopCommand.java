package net.tavania.planetcloudbusiness.launcher.command;

import net.tavania.planetcloudbusiness.logger.ConsoleLogger;
import net.tavania.planetcloudbusiness.logger.LoggerType;
import net.tavania.planetcloudbusiness.logger.SQLLogger;
import net.tavania.planetcloudbusiness.main.PlanetCloud;

import java.util.Date;
import java.util.GregorianCalendar;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.planetcloudbusiness.launcher.command
    Date: 02.11.2019
    
*/
public class StopCommand extends Command {
    public StopCommand(String name, String... alias) {
        super(name, alias);
    }

    @Override
    public void execute(ConsoleLogger logger, String name, String... args) {
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Stopping system...");
        Date date = new Date();
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setGregorianChange(date);
        if (PlanetCloud.getInstance().getMySQLConnectionHandler().c != null) {
            SQLLogger.getInstance().log(LoggerType.SUCCESS, "Stopping system by using command.");
        }
        for (Thread thread : PlanetCloud.getInstance().getTavaniaThread().getRunningThreads()) {
            thread.stop();
        }
        System.exit(1);
    }
}
