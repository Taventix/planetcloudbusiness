package net.tavania.planetcloudbusiness.launcher.command;

import net.tavania.planetcloudbusiness.logger.ConsoleLogger;
import net.tavania.planetcloudbusiness.logger.LoggerType;
import net.tavania.planetcloudbusiness.main.PlanetCloud;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness - Server/net.tavania.planetcloudbusiness.launcher.command
    Date: 24.12.2019
    
*/
public class ReadyCommand extends Command {
    public ReadyCommand(String name, String... alias) {
        super(name, alias);
    }

    @Override
    public void execute(ConsoleLogger logger, String name, String... args) {
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "STARTING FILE-SERVER!");
        ConsoleLogger.getInstance().log(LoggerType.WARNING, "No commands available!");
        PlanetCloud.getInstance().getNettyServerConnection().startServer(PlanetCloud.getInstance().getNettyServerConnection().getPort(), true);
    }
}
