package net.tavania.planetcloudbusiness.launcher.command;

import net.tavania.planetcloudbusiness.logger.ConsoleLogger;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.planetcloudbusiness.launcher.command
    Date: 07.10.2019
    
*/
public abstract class Command {

    private final String name;
    private final String[] alias;


    public Command(String name, String... alias) {
        this.name = name;
        this.alias = alias;
    }

    public abstract void execute(final ConsoleLogger logger, final String name, final String... args);

    public String getName() {
        return name;
    }

    public String[] getAlias() {
        return alias;
    }
}
