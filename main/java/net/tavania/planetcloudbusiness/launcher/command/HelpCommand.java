package net.tavania.planetcloudbusiness.launcher.command;

import net.tavania.planetcloudbusiness.logger.ConsoleLogger;
import net.tavania.planetcloudbusiness.logger.LoggerType;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.planetcloudbusiness.launcher.command
    Date: 07.10.2019
    
*/
public class HelpCommand extends Command {

    public HelpCommand(String name, String... alias) {
        super(name, alias);
    }

    @Override
    public void execute(ConsoleLogger logger, String name, String... args) {
        if (args.length == 0) {
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "List of all available commands:");
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "  » help | List of all commands");
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "  » user | Manage users [SOON!]");
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "  » location | Manage locations");
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "  » project | Manage project");
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "  » thread | See all threads");
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "  » status | General status");
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "  » stop | Stops the software");
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "  » version | Compare the installed version");
        } else {
            ConsoleLogger.getInstance().log(LoggerType.ERROR, "Wrong arguments!");
        }
    }
}
