package net.tavania.planetcloudbusiness.launcher.command;

import net.tavania.planetcloudbusiness.logger.ConsoleLogger;
import net.tavania.planetcloudbusiness.logger.LoggerType;
import net.tavania.planetcloudbusiness.main.PlanetCloud;
import net.tavania.planetcloudbusiness.service.Privacy;
import net.tavania.planetcloudbusiness.service.location.TavaniaLocation;
import net.tavania.planetcloudbusiness.service.project.TavaniaProject;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness - Server/net.tavania.planetcloudbusiness.launcher.command
    Date: 21.12.2019
    
*/
public class ProjectCommand extends Command {
    public ProjectCommand(String name, String... alias) {
        super(name, alias);
    }

    @Override
    public void execute(ConsoleLogger logger, String name, String... args) {
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("list")) {
                if (TavaniaLocation.getTavaniaLocations().size() != 0) {
                    ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "List of all projects:");
                    for (int i = 0; i < TavaniaProject.getTavaniaProjects().size(); i++) {
                        TavaniaProject tavaniaProject = TavaniaProject.getTavaniaProjects().get(i);
                        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "» " + tavaniaProject.getId() + " | " + tavaniaProject.getName() + " | " + tavaniaProject.getDate() + " | " + tavaniaProject.getProjectPrivacy());
                    }
                } else {
                    ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "No projects created yet.");
                }
            }
        } else if (args.length == 4) {
            if (args[0].equalsIgnoreCase("create")) {
                String privacy;
                if (args[2].equalsIgnoreCase("PRIVATE") | args[2].equalsIgnoreCase("INTERNAL") | args[2].equalsIgnoreCase("PUBLIC")) {
                    privacy = args[2];
                } else {
                    ConsoleLogger.getInstance().log(LoggerType.ERROR, "Wrong privacy! [PRIVATE, INTERNAL, PUBLIC]");
                    return;
                }
                new TavaniaProject(TavaniaProject.getTavaniaProjects().size() + 1, args[1], Privacy.valueOf(privacy), PlanetCloud.getInstance().getGregorianDate());
                ConsoleLogger.getInstance().log(LoggerType.SUCCESS, "Successfully created new project!");
            }
        } else {
            ConsoleLogger.getInstance().log(LoggerType.ERROR, "Wrong arguments!");
        }
    }
}
