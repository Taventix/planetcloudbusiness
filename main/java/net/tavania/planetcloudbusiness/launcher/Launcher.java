package net.tavania.planetcloudbusiness.launcher;

import net.tavania.planetcloudbusiness.launcher.command.*;
import net.tavania.planetcloudbusiness.launcher.thread.FirstStartThread;
import net.tavania.planetcloudbusiness.launcher.thread.StartingThread;
import net.tavania.planetcloudbusiness.launcher.thread.UpdateThread;
import net.tavania.planetcloudbusiness.logger.ConsoleLogger;
import net.tavania.planetcloudbusiness.logger.LoggerType;
import net.tavania.planetcloudbusiness.main.PlanetCloud;

import java.io.File;
import java.util.Scanner;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.launcher
    Date: 29.09.2019
    
*/
public class Launcher {

    /**
     * Starting all available threads which are important for the start of the application
     */
    public void start() {
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "\n" +
                "________                           _____       ______________            _________\n" +
                "___  __/_____ ___   _______ __________(_)_____ __  ____/__  /_________  _______  /\n" +
                "__  /  _  __ `/_ | / /  __ `/_  __ \\_  /_  __ `/  /    __  /_  __ \\  / / /  __  / \n" +
                "_  /   / /_/ /__ |/ // /_/ /_  / / /  / / /_/ // /___  _  / / /_/ / /_/ // /_/ /  \n" +
                "/_/    \\__,_/ _____/ \\__,_/ /_/ /_//_/  \\__,_/ \\____/  /_/  \\____/\\__,_/ \\__,_/   \n" +
                "                                                                                  \n");
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Starting application...");
        File file = new File("plugins");
        PlanetCloud.getInstance().getTavaniaThread().startThread(new UpdateThread(), "UpdateThread");
        if (file.exists()) {
            PlanetCloud.getInstance().getTavaniaThread().startThread(new StartingThread(), "StartingThread");
        } else {
            PlanetCloud.getInstance().getTavaniaThread().startThread(new FirstStartThread(), "FirstStartThread");
        }
        PlanetCloud.getInstance().getCommandExecuter().register(new HelpCommand("help"));
        PlanetCloud.getInstance().getCommandExecuter().register(new LocationCommand("location"));
        PlanetCloud.getInstance().getCommandExecuter().register(new ProjectCommand("project"));
        PlanetCloud.getInstance().getCommandExecuter().register(new ReadyCommand("ready"));
        PlanetCloud.getInstance().getCommandExecuter().register(new StatusCommand("status"));
        PlanetCloud.getInstance().getCommandExecuter().register(new StopCommand("stop"));
        PlanetCloud.getInstance().getCommandExecuter().register(new ThreadCommand("thread"));
        while (true) {
            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine();
            boolean exists = PlanetCloud.getInstance().getCommandExecuter().dispatchCommand(input);
            if (!exists)
                ConsoleLogger.getInstance().log(LoggerType.ERROR, "Command not found! Use 'help' for more informations.");

        }
    }

}
