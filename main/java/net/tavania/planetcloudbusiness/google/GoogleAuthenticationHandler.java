package net.tavania.planetcloudbusiness.google;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;
import net.tavania.planetcloudbusiness.logger.ConsoleLogger;
import net.tavania.planetcloudbusiness.logger.LoggerType;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.planetcloudbusiness.google
    Date: 21.10.2019
    
*/
public class GoogleAuthenticationHandler {

    private boolean isAuthorized = true;
    private GoogleAuthenticator googleAuthenticator = new GoogleAuthenticator();
    private GoogleAuthenticatorKey googleAuthenticatorKey = googleAuthenticator.createCredentials();
    private String googleAuthKey;
    private int keyPW;

    public boolean isAuthorized() {
        return isAuthorized;
    }

    public void setAuthorized(boolean locked) {
        isAuthorized = locked;
    }

    public void a() {
        String key = googleAuthenticatorKey.getKey();
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Your 2FA-Code is: " + key);
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Please type in that code in the Google Authenticator App to authorize.");
        setGoogleAuthKey(key);
        System.out.println(getGoogleAuthKey());
    }

    public void b(int code) {
        isAuthorized = googleAuthenticator.authorize(googleAuthenticatorKey.getKey(), code);
        if (isAuthorized) {
            ConsoleLogger.getInstance().log(LoggerType.SUCCESS, "Successfully authorized!");
        } else {
            ConsoleLogger.getInstance().log(LoggerType.ERROR, "Error during authentication process! Please restart the application.");
            System.exit(0);
        }
    }


    public String getGoogleAuthKey() {
        return googleAuthKey;
    }

    public void setGoogleAuthKey(String googleAuthKey) {
        this.googleAuthKey = googleAuthKey;
    }

    public int getKeyPW() {
        return keyPW;
    }
}
