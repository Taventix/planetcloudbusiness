package net.tavania.planetcloudbusiness.json;

import net.tavania.planetcloudbusiness.database.DatabaseType;
import net.tavania.planetcloudbusiness.logger.ConsoleLogger;
import net.tavania.planetcloudbusiness.logger.LoggerType;
import net.tavania.planetcloudbusiness.main.PlanetCloud;

import java.util.Scanner;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.planetcloudbusiness.json
    Date: 04.11.2019
    
*/
public class JsonConfigurationHandler {

    private String databaseType;
    private String hostname;
    private String username;
    private int port;
    private String password;

    public void configureDatabase() {
        ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Are you using a database? [YES, NO]");
        Scanner scanner = new Scanner(System.in);
        String nextLine = scanner.nextLine();
        if (nextLine.equalsIgnoreCase("YES")) {
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "Which database are you using? [MySQL, Redis]");
            Scanner scanner1 = new Scanner(System.in);
            String nextLine1 = scanner1.nextLine();
            if (nextLine1.equalsIgnoreCase(DatabaseType.MySQL.toString())) {
                setDatabaseType(nextLine1);
            } else if (nextLine1.equalsIgnoreCase(DatabaseType.Redis.toString())) {
                setDatabaseType(nextLine1);
            } else {
                ConsoleLogger.getInstance().log(LoggerType.ERROR, "Database not available. Try again!");
                return;
            }
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "What's the hostname of the Database-Server? [default: localhost]");
            Scanner scanner2 = new Scanner(System.in);
            String nextLine2 = scanner2.nextLine();
            setHostname(nextLine2);
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "What's the port of the Database-Server? [default: 3306]");
            Scanner scanner3 = new Scanner(System.in);
            String nextLine3 = scanner3.nextLine();
            try {
                setPort(Integer.parseInt(nextLine3));
            } catch (NumberFormatException e) {
                ConsoleLogger.getInstance().log(LoggerType.ERROR, "The port can only be a number. Try again!");
                return;
            }
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "What's the username of the Database-Server? [MySQL: root] [Redis: not important]");
            Scanner scanner4 = new Scanner(System.in);
            String nextLine4 = scanner4.nextLine();
            setUsername(nextLine4);
            ConsoleLogger.getInstance().log(LoggerType.INFORMATION, "What's the password of the Database-Server?");
            Scanner scanner5 = new Scanner(System.in);
            String nextLine5 = scanner5.nextLine();
            setPassword(nextLine5);
            ConsoleLogger.getInstance().log(LoggerType.SUCCESS, "Saving data...");
            PlanetCloud.getInstance().getJsonWebDataHandler().setJsonString(PlanetCloud.getInstance().getLocation() + "/Controller/config.json", "DatabaseType", this.databaseType);
            PlanetCloud.getInstance().getJsonWebDataHandler().setJsonString(PlanetCloud.getInstance().getLocation() + "/Controller/config.json", "Hostname", this.hostname);
            PlanetCloud.getInstance().getJsonWebDataHandler().setJsonInteger(PlanetCloud.getInstance().getLocation() + "/Controller/config.json", "Port", this.port);
            PlanetCloud.getInstance().getJsonWebDataHandler().setJsonString(PlanetCloud.getInstance().getLocation() + "/Controller/config.json", "Username", this.username);
            PlanetCloud.getInstance().getJsonWebDataHandler().setJsonString(PlanetCloud.getInstance().getLocation() + "/Controller/config.json", "Password", this.password);
            ConsoleLogger.getInstance().log(LoggerType.SUCCESS, "Saved all data successfully.");
            return;
        } else {
            PlanetCloud.getInstance().getJsonWebDataHandler().setJsonString(PlanetCloud.getInstance().getLocation() + "/Controller/config.json", "DatabaseType", "NONE");
            ConsoleLogger.getInstance().log(LoggerType.SUCCESS, "No database configured, using JSON.");
        }
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDatabaseType() {
        return databaseType;
    }

    public void setDatabaseType(String databaseType) {
        this.databaseType = databaseType;
    }
}
