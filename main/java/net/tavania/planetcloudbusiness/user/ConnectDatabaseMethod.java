package net.tavania.planetcloudbusiness.user;

import net.tavania.planetcloudbusiness.main.PlanetCloud;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.planetcloudbusiness.user
    Date: 13.11.2019
    
*/
public class ConnectDatabaseMethod extends TavaniaMethod {
    @Override
    public void run() {
        if (PlanetCloud.getInstance().getJsonWebDataHandler().getString(PlanetCloud.getInstance().getLocation() + "/Controller/config.json", "DatabaseType") == null) {
            PlanetCloud.getInstance().setDatabase("NONE");
            return;
        }
        if (PlanetCloud.getInstance().getJsonWebDataHandler().getString(PlanetCloud.getInstance().getLocation() + "/Controller/config.json", "DatabaseType").equals("MySQL")) {
            PlanetCloud.getInstance().setDatabase("MySQL");
        } else if (PlanetCloud.getInstance().getJsonWebDataHandler().getString(PlanetCloud.getInstance().getLocation() + "/Controller/config.json", "DatabaseType").equals("REDIS")) {
            PlanetCloud.getInstance().setDatabase("REDIS");
        } else {
            PlanetCloud.getInstance().setDatabase("NONE");
        }
    }

    @Override
    public void save() {

    }
}
