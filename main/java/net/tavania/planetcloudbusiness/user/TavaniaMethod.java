package net.tavania.planetcloudbusiness.user;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.planetcloudbusiness.user
    Date: 13.11.2019
    
*/
public abstract class TavaniaMethod {

    public abstract void run();

    public abstract void save();

}
