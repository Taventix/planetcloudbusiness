package net.tavania.planetcloudbusiness.main;

import net.tavania.planetcloudbusiness.database.mysql.*;
import net.tavania.planetcloudbusiness.database.redis.RedisConnectionHandler;
import net.tavania.planetcloudbusiness.google.GoogleAuthenticationHandler;
import net.tavania.planetcloudbusiness.json.JsonConfigurationHandler;
import net.tavania.planetcloudbusiness.json.JsonWebDataHandler;
import net.tavania.planetcloudbusiness.launcher.Launcher;
import net.tavania.planetcloudbusiness.launcher.command.CommandExecuter;
import net.tavania.planetcloudbusiness.launcher.thread.TavaniaThread;
import net.tavania.planetcloudbusiness.netty.NettyConfigurationHandler;
import net.tavania.planetcloudbusiness.netty.NettyServerConnection;
import net.tavania.planetcloudbusiness.netty.handler.NettyServerHandler;
import net.tavania.planetcloudbusiness.service.project.TavaniaProjectHandler;
import net.tavania.planetcloudbusiness.yaml.YamlFileHandler;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

/*

    Copyright © 2019 Alexander F.
    Twitter: @Taventiksch
    Location: PlanetCloudBusiness/net.tavania.main
    Date: 29.09.2019
    
*/
public class PlanetCloud {

    private static PlanetCloud instance = new PlanetCloud();
    public static PlanetCloud getInstance() {
        return instance;
    }

    public static Launcher launcher = new Launcher();
    private TavaniaThread tavaniaThread = new TavaniaThread();
    public static String VERSION = "B-0.5";
    private JsonWebDataHandler jsonWebDataHandler = new JsonWebDataHandler();
    private TavaniaProjectHandler tavaniaProjectHandler = new TavaniaProjectHandler();
    private CommandExecuter commandExecuter = new CommandExecuter();
    private YamlFileHandler yamlFileHandler = new YamlFileHandler();
    private MySQLConnectionHandler mySQLConnectionHandler = new MySQLConnectionHandler();
    private LocationSQLHandler locationSQLHandler = new LocationSQLHandler();
    private ProjectSQLHandler projectSQLHandler = new ProjectSQLHandler();
    private UserSQLHandler userSQLHandler = new UserSQLHandler();
    private ArrayList<String> locationNames = new ArrayList<>();
    private String currentLocationName;
    private GoogleAuthenticationHandler googleAuthenticationHandler = new GoogleAuthenticationHandler();
    private RedisConnectionHandler redisConnectionHandler = new RedisConnectionHandler();
    private NettyServerConnection nettyServerConnection = new NettyServerConnection();
    private LogSQLHandler logSQLHandler = new LogSQLHandler();
    private JsonConfigurationHandler jsonConfigurationHandler = new JsonConfigurationHandler();
    private NettyConfigurationHandler nettyConfigurationHandler = new NettyConfigurationHandler();
    private NettyServerHandler nettyServerHandler = new NettyServerHandler();

    private String location;
    private String database;
    private String clientName;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public static void main(String[] args) {
        getLauncher().start();
    }

    public static Launcher getLauncher() {
        return launcher;
    }

    public TavaniaThread getTavaniaThread() {
        return tavaniaThread;
    }

    public JsonWebDataHandler getJsonWebDataHandler() {
        return jsonWebDataHandler;
    }

    public TavaniaProjectHandler getTavaniaProjectHandler() {
        return tavaniaProjectHandler;
    }

    public CommandExecuter getCommandExecuter() {
        return commandExecuter;
    }

    public YamlFileHandler getYamlFileHandler() {
        return yamlFileHandler;
    }

    public MySQLConnectionHandler getMySQLConnectionHandler() {
        return mySQLConnectionHandler;
    }

    public LocationSQLHandler getLocationSQLHandler() {
        return locationSQLHandler;
    }

    public ProjectSQLHandler getProjectSQLHandler() {
        return projectSQLHandler;
    }

    public UserSQLHandler getUserSQLHandler() {
        return userSQLHandler;
    }

    public ArrayList<String> getLocationNames() {
        return locationNames;
    }

    public String getCurrentLocationName() {
        return currentLocationName;
    }

    public void setCurrentLocationName(String currentLocationName) {
        this.currentLocationName = currentLocationName;
    }

    public GoogleAuthenticationHandler getGoogleAuthenticationHandler() {
        return googleAuthenticationHandler;
    }

    public RedisConnectionHandler getRedisConnectionHandler() {
        return redisConnectionHandler;
    }


    public NettyServerConnection getNettyServerConnection() {
        return nettyServerConnection;
    }

    public LogSQLHandler getLogSQLHandler() {
        return logSQLHandler;
    }

    public JsonConfigurationHandler getJsonConfigurationHandler() {
        return jsonConfigurationHandler;
    }

    public NettyConfigurationHandler getNettyConfigurationHandler() {
        return nettyConfigurationHandler;
    }

    public NettyServerHandler getNettyServerHandler() {
        return nettyServerHandler;
    }


    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Date getGregorianDate() {
        Date date = new Date();
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setGregorianChange(date);
        return gregorianCalendar.getTime();
    }

}
